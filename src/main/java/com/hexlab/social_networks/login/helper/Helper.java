package com.hexlab.social_networks.login.helper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hexlab.social_networks.R;

/**
 * Helper class which contains commons methods
 *
 * Created by oreste on 15/01/16.
 */
public class Helper {

    private static final String URL_PLAY_SERVICES = "https://play.google.com/store/apps/details?id=com.google.android.gms&hl=it";

    /**
     * checks if the play services is available and if it is not, then will prompt an error dialog
     *
     * @param activity
     *
     * @return true if the play services is available, false otherwise
     */
    public static boolean checkIfPlayServicesIsAvailable(final Activity activity){
        if(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity)!= ConnectionResult.SUCCESS){
            new MaterialDialog.Builder(activity).content(R.string.hint_txt_error_play_services)
                    .positiveText(R.string.hint_txt_ok)
                    .positiveColorRes(R.color.main_color)
                    .backgroundColorRes(android.R.color.white)
                    .contentColorRes(R.color.main_color)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                            activity.startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(URL_PLAY_SERVICES)));
                        }
                    }).show();

            return false;
        }

        return true;
    }
}
