package com.hexlab.social_networks.login;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.view.LayoutInflater;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import com.hexlab.social_networks.R;
import com.hexlab.social_networks.databinding.WebDialogBinding;

/**
 * Created by oreste on 05/10/15.
 *
 * Class which shows a web dialog where the user can login to social networks
 *
 */
public class WebDialog {

    public ObservableBoolean visibleProgress = new ObservableBoolean(true);
    private MaterialDialog materialDialog;
    private WebDialogBinding webDialogBinding;
    private WebViewClient webViewClient;

    /**
     * called when the oauth_verifier is catched
     */
    public interface OnOauthListener{
        void onOauthReceived(String token);
    }

    /**
     * shows a web dialog where the user can login to it's own SocialNetwork
     *
     * @param context
     * @param url
     * @param color
     * @param onOauthListener
     */
    protected void showWebDialog(final Context context, String url, @ColorRes int color, final OnOauthListener onOauthListener){
        webDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.web_dialog, null, false);
        webDialogBinding.setBinding(this);
        webDialogBinding.web.setWebViewClient(getWebViewClientInstance(onOauthListener));
        webDialogBinding.web.loadUrl(url);
        webDialogBinding.progressView.setColor(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? context.getColor(color) : context.getResources().getColor(color));

        materialDialog = new MaterialDialog.Builder(context).customView(webDialogBinding.getRoot(),false).cancelable(false).show();
    }

    /**
     * destroys objects and unbind views
     */
    protected void destroy(){
        webDialogBinding.setBinding(null);
        webDialogBinding.unbind();
    }

    /**
     *
     * @param onOauthListener
     * @return a WebViewClient instance in order to retrieves the Oauth_verifier
     */
    private WebViewClient getWebViewClientInstance(final OnOauthListener onOauthListener){
        if(webViewClient==null){
            webViewClient = new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    visibleProgress.set(true);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    visibleProgress.set(false);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    onOauthListener.onOauthReceived(url.substring(url.indexOf("oauth_verifier=") + 15));
                    webDialogBinding.setBinding(null);
                    webDialogBinding.unbind();
                    materialDialog.dismiss();
                    return true;
                }
            };
        }
        return webViewClient;
    }

}
