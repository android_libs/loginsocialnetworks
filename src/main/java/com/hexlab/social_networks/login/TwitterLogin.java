package com.hexlab.social_networks.login;

import android.app.Activity;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.RequestToken;


/**
 * Created by oreste on 05/10/15.
 *
 * It allows the user to logs in with his own Twitter account
 *
 */
public class TwitterLogin {

    private static RequestToken requestToken;
    private static WebDialog webDialog;
    private static Twitter twitter;

    /**
     * called when the user's info are being retrieved
     */
    public interface UserInfoListener{
        void onInfoRetrieved(long id, String username);
        void onError();
    }

    /**
     * shows a web dialog where the user can put his own credentials to login inside Twitter
     *
     * @param activity
     * @param apiKey
     * @param apiSecret
     * @param textWait
     * @param color
     * @param onOauthListener
     */
    public static void login(final Activity activity, final String apiKey, final String apiSecret, @StringRes final int textWait, @ColorRes final int color, final WebDialog.OnOauthListener onOauthListener){
        final MaterialDialog materialDialog = new MaterialDialog.Builder(activity).content(textWait).widgetColorRes(color).progress(true,0).show();
        Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {
                try {
                    if(twitter==null){
                        twitter = new TwitterFactory().getInstance();
                        twitter.setOAuthConsumer(apiKey, apiSecret);
                        requestToken = twitter.getOAuthRequestToken("http://hexlabsoftware.com");
                    }
                } catch (TwitterException e) {}
                return "ok";
            }
        }).continueWith(new Continuation<String, Object>() {
            @Override
            public Object then(Task<String> task) throws Exception {
                materialDialog.dismiss();
                if(webDialog!=null){
                    webDialog.showWebDialog(activity,requestToken.getAuthenticationURL(),color,onOauthListener);
                }
                (webDialog = new WebDialog()).showWebDialog(activity, requestToken.getAuthenticationURL(), color, onOauthListener);
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);
    }

    /**
     *
     * @param token_secret
     * @param userInfoListener
     *
     * @return the user's infos, id and screen name
     */
    public static void getUserInfos(final String token_secret, final UserInfoListener userInfoListener){
        final String ID = "id", NAME = "name";
        if(twitter==null) return;
        Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {
                try {
                    twitter.setOAuthAccessToken(twitter.getOAuthAccessToken(requestToken,token_secret));
                    User user = twitter.verifyCredentials();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(NAME,user.getScreenName());
                    jsonObject.put(ID,user.getId());
                    return jsonObject.toString();
                } catch (TwitterException | JSONException e) {}
                return null;
            }
        }).continueWith(new Continuation<String, Object>() {
            @Override
            public Object then(Task<String> task) throws Exception {
                if(task.getResult()!=null){
                    try {
                        JSONObject jsonObject = new JSONObject(task.getResult());
                        userInfoListener.onInfoRetrieved(jsonObject.getLong(ID),jsonObject.getString(NAME));
                    } catch (JSONException e) {}
                }
                return null;
            }
        });
    }

    /**
     * destroys objects
     */
    public static void destroy(){
        if(webDialog==null) return;
        webDialog.destroy();
        requestToken = null;
        webDialog = null;
        twitter = null;
    }

}
