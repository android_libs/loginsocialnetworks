package com.hexlab.social_networks.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by oreste on 05/10/15.
 *
 * it allows the user logs in Facebook
 *
 */
public class Facebook {


    /**
     * starts the login flow for Facebook
     *
     * @param activity
     */
    public static CallbackManager login(final Object activity, FacebookCallback<LoginResult> facebookCallback){
        CallbackManager callBackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callBackManager, facebookCallback);

        if(activity instanceof Activity){
            LoginManager.getInstance().logInWithReadPermissions((Activity) activity, Arrays.asList("email","user_about_me"));
        }else{
            LoginManager.getInstance().logInWithReadPermissions((Fragment) activity, Arrays.asList("email","user_about_me"));
        }

        return callBackManager;
    }

    /**
     * gets infos about the Facebook user
     *
     * @param loginResult
     * @param params
     * @param callback
     */
    public static void getMeRequest(LoginResult loginResult, String params, final GraphRequest.GraphJSONObjectCallback callback){
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                callback.onCompleted(object, response);

            }
        });
        request.setParameters(getParamsMeRequest(params));
        request.executeAsync();
    }

    /**
     * when the login is done it is called to fetch the access_token
     *
     * @param requestCode
     * @param resultCode
     * @param data
     *
     * @return the access_token or null if none is present
     */
    public static void onActivityResult(CallbackManager callbackManager, int requestCode, int resultCode, Intent data){
        if(callbackManager==null) return;
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     *
     * Bundle needed to get right fields
     *
     * @param params
     * @return a bundle with fields filled
     */
    private static Bundle getParamsMeRequest(String params){
        Bundle bundle = new Bundle();
        bundle.putString("fields","email"+params);
        return bundle;
    }

}
